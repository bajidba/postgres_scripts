#!/bin/bash
/opt/PostgreSQL/9.1/bin/pg_ctl status -D /export/pgdata/9.1/data > /dev/null 2>&1
rtn=$?
#echo "..Executed.." >>/tmp/pgping.log
if [ $rtn -ne 0 ]; then
  echo ALERT::PG server is NOT running > /tmp/pgruncheck.log
  mailx -s " `hostname`::HIGH ALERT::PG Server NOT running" abdul.sayeed@enterprisedb.com < /tmp/pgruncheck.log
else
  /opt/PostgreSQL/9.1/bin/psql -d postgres -U postgres -t -p 5433 -c "Select pg_postmaster_start_time();"  >/dev/null
  chk=$?
fi
if [ $chk -gt 0 ]; then
  echo "ALERT::Connection count exceeded, Cant connect to the server" > /tmp/pgconn.log
  mailx -s " `hostname` :: ALERT::Max connection Count reached" abdul.sayeed@enterprisedb.com </tmp/pgconn.log
else
  MAXCONN=`/opt/PostgreSQL/9.1/bin/psql -d postgres -U postgres -t -p 5433 -c "show max_connections;"`
  CONN=`/opt/PostgreSQL/9.1/bin/psql -d postgres -U postgres -t -p 5433 -c "select count(*) from pg_stat_activity;"`
  CHECK=1850
  if [ $CONN -gt $CHECK ]; then
    mailx -s "Alert:: `hostname` Connection Count Exceeded Threshold value" abdul.sayeed@enterprisedb.com < /tmp/pgconn.log
  fi
fi


#!/bin/bash
PGPASSWORD=postgres
CONN_COUNT=`/usr/pgsql-9.3/bin/psql -Aqt -p 5432 -U postgres -d MEAL_DB -c "select count(1) from pg_stat_activity;"`
if [ $CONN_COUNT -gt 65 -a $CONN_COUNT -le 75 ]; then
  /usr/pgsql-9.3/bin/psql -p 5432 -U postgres -d MEAL_DB -c "select datname,pid,usename, application_name, client_addr, client_hostname from pg_stat_activity where pid<>pg_backend_pid();" > /tmp/connections_warning.out
  /usr/pgsql-9.3/bin/psql -p 5432 -U postgres -d MEAL_DB -c "insert into snapshots.track_connections select *, now() from pg_stat_activity where pid<>pg_backend_pid();" > /dev/null
  mailx -s " `hostname` :: WARNING:: Connections to the database are $CONN_COUNT, details are below. For more details, look at snapshots.track_connections table" baji.shaik@openscg.com < /tmp/connections_warning.out
fi
if [ $CONN_COUNT -gt 75 ]; then

/usr/pgsql-9.3/bin/psql -p 5432 -U postgres -d MEAL_DB -c "select datname,pid,usename, application_name, client_addr, client_hostname from pg_stat_activity where pid<>pg_backend_pid();" > /tmp/connetions_critical.out
  /usr/pgsql-9.3/bin/psql -p 5432 -U postgres -d MEAL_DB -c "insert into snapshots.track_connections select *, now() from pg_stat_activity where pid<>pg_backend_pid();" > /dev/null
  mailx -s " `hostname` :: CRITICAL:: Connections to the database are $CONN_COUNT, details are below. For more details, look at snapshots.track_connections table" baji.shaik@openscg.com < /tmp/connections_critical.out
fi

