#!/bin/bash

PSQL=/opt/PostgreSQL/9.1/bin
Primary_Host=ip
Port=5432
Dbname=postgres

Primary_Xlog_Loc=$($PSQL/psql -Atq -c 'select pg_xlogfile_name(pg_current_xlog_location()::text)' -p $Port -h $Primary_Host $Dbname)

Slave_Xlog_Replay=$($PSQL/psql -Atq -c 'select pg_last_xlog_replay_location();' -p  $Port   $Dbname )

Slave_Xlog_Replay_File=$($PSQL/psql -Atq -c "select pg_xlogfile_name('$Slave_Xlog_Replay'::text)" -p $Port -h $Primary_Host $Dbname)

Standby_Delay=$($PSQL/psql -Atq -c "select trunc(public.HOT_STANDBY_LAG('$Primary_Xlog_Loc','$Slave_Xlog_Replay_File'))" -p $Port -h $Primary_Host $Dbname
)


