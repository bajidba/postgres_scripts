#!/bin/sh
CPPATH=/home/postgres/check_postgres.pl
LOGFILE=/tmp/monitor.log
PGPORT=5433
PGUSER=postgres
rm -f $LOGFILE
for dbname in `/opt/PostgreSQL/9.1/bin/psql -p $PGPORT -d postgres -Atc"select datname from pg_database where datname not in ('template0','template1');"`;
do
   ### Action for VACUUM
   ## Commented due to autovacuum parameters has been tuned based on the customer request
   # if [ `$CPPATH -u $PGUSER -db $dbname  -p $PGPORT --action last_vacuum --warning='3d' --critical='5d' | gawk '{print $2}'` != "OK:" ]; then
   #   $CPPATH -u $PGUSER -db $dbname  -p $PGPORT --action last_vacuum --warning='3d' --critical='5d' | gawk '{print substr($0,0,126) }' >> $LOGFILE
   #fi
   ### Action for max_connection check with number of connections
   if [ `$CPPATH -u $PGUSER -db $dbname  -p $PGPORT --warning=2300 --critical=2400 --action backends | gawk '{print $2}'` != "OK:" ]; then
      $CPPATH -u $PGUSER -db $dbname  -p $PGPORT --warning=2300 --critical=2400 --action backends | gawk '{print substr($0,0,126) }' >> $LOGFILE
   fi
   #### Action for archive_ready tells how many .ready files existing in pg_xlog/archive_status directory #####
   ### This value calculated based on the formula (2 + checkpoint_completion_target) * checkpoint_segments +1 = 270
   if [ `$CPPATH -u $PGUSER -db $dbname  -p $PGPORT --critical=250 --action archive_ready | gawk '{print $2}'` != "OK:" ]; then
      $CPPATH -u $PGUSER -db $dbname  -p $PGPORT --critical=250 --action archive_ready | gawk '{print substr($0,0,126) }' >> $LOGFILE
   fi
   ##### Action for txn_wraparound
   if [ `$CPPATH -u $PGUSER -db $dbname  -p $PGPORT --action txn_wraparound | gawk '{print $2}'` != "OK:" ]; then
      $CPPATH -u $PGUSER -db $dbname  -p $PGPORT --action txn_wraparound | gawk '{print substr($0,0,126) }' >> $LOGFILE
   fi
   ### Action for pg_xlog growth
   if [ `$CPPATH -u $PGUSER -db $dbname  -p $PGPORT --action wal_files --warning=300 --critical=400 | gawk '{print $2}'` != "OK:" ]; then
      $CPPATH -u $PGUSER -db $dbname  -p $PGPORT --action wal_files --warning=300 --critical=400 | gawk '{print substr($0,0,126) }' >> $LOGFILE
   fi
done;
if [ -f "$LOGFILE" ]; then
  /usr/bin/mailx -s "`hostname` Server - Alert Report ::" mailid < $LOGFILE
fi
