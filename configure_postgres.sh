#!/bin/bash
export OS=$1
export VERSION=$2
export PORT=$3
export INSTALL_DIR=$HOME/pg_software

if [ ! -d "$INSTALL_DIR" ]
then
    echo "$INSTALL_DIR directory doesn't exist. Creating now"
    mkdir $INSTALL_DIR
    echo "$INSTALL_DIR directory created"
else
    echo "$INSTALL_DIR exists"
fi

cd $HOME/pg_software

## Downloading the source code as per OS

if [ "$1" == "Linux" ] || [ "$1" == "linux" ]; then
	echo "Downloading PostgreSQL $VERSION.."
   wget https://ftp.postgresql.org/pub/source/v$VERSION/postgresql-$VERSION.tar.bz2 >/dev/null 
   if [ "$?" -gt "0" ]; then
   	  echo "Could not download the file, check if wget is installed or not"
      exit
    else
      echo "Downloaded PostgreSQL $VERSION..."
   fi
elif [ "$1" == "Mac" ] || [ "$1" == "mac" ]; then
	echo "Downloading PostgreSQL $VERSION.."
   curl -O https://ftp.postgresql.org/pub/source/v$VERSION/postgresql-$VERSION.tar.bz2 > $HOME/pg_software/comile_$VERSION.log 2>&1 
   if [ "$?" -gt "0" ]; then
   	  echo "Could not download the file, check if curl is installed or not"
      exit
   else
   	  echo "Downloaded PostgreSQL $VERSION..."
   fi

else
	echo "currenly it works with linux and mac OSes"
	exit
fi

## Compiling the source code

echo "wait ! let it compile..."
cd $INSTALL_DIR/
tar -xf postgresql-$VERSION.tar.bz2
cd postgresql-$VERSION
./configure --prefix=$HOME/pg_software/$VERSION && make -j 8 && make install > $HOME/compile.log
if [ "$?" -gt "0" ]; then
   	  echo "Could not compile the source, please look at compile.log for more information"
      exit
else
     echo "Ok, compiled it for you !!.."
fi

## Setting up evn
echo "setting up env...it makes your life easy.. "

touch $HOME/pg_software/$VERSION/source_$VERSION.env
echo "export PATH=$HOME/pg_software/$VERSION/bin:$PATH" >> $HOME/pg_software/$VERSION/source.env
echo "export PGPORT=$PORT" >> $HOME/pg_software/$VERSION/source.env
echo "export PGDATA=$HOME/pg_software/$VERSION/data" >> $HOME/pg_software/$VERSION/source.env
echo "export PGDATABASE=postgres" >> $HOME/pg_software/$VERSION/source.env
echo "export PGUSER=postgres" >> $HOME/pg_software/$VERSION/source.env

echo "source $HOME/pg_software/$VERSION/source.env" >> $HOME/.bash_profile
. $HOME/.bash_profile

echo "Ok, done with env setup.. now you can be lazy.. !"
## Create DATA directory

echo "Oh, it's data time.. creating data directory...!"

initdb -D $PGDATA -U postgres >/dev/null 2>&1

echo "Done buddy !! .. let me enable logging and allow connections from other hosts.....I would love if it's by default, but it's not !"
## Enable logging.. and allow other hosts.

echo "port=$PORT" >> $PGDATA/postgresql.conf
echo "logging_collector=on" >> $PGDATA/postgresql.conf
echo "listen_addresses='*'" >> $PGDATA/postgresql.conf

## Start database
echo "you are having coffee !!!.. Ok, np, let me start it for you..!"
pg_ctl -D $PGDATA start >/dev/null 
sleep 5
echo "hmm.. seems it started.. but let me check once.."
## check DB is up

psql -p $PGPORT -U $PGUSER -d $PGDATABASE -c "select 1;" > /dev/null

if [ "$?" -gt "0" ]; then
	echo "Sorry !!, it's not started.. I'm not yet smart enough to fix.. :-(.. blame the author !!.."
else
	echo "hurray.. it works.. enjoy!"
fi
