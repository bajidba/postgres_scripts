#!/bin/bash

MAILFILE=/tmp/daily-monitoring.log
if [ -f "$MAILFILE" ]; then
   rm -f $MAILFILE
fi

dialymonitoring() {

#########################################################################################
### Setting Environment Variables
#########################################################################################
export LD_LIBRARY_PATH=/opt/PostgreSQL/9.1/lib:$LD_LIBRARY_PATH
BIN=/opt/PostgreSQL/9.1/bin
PGDATA=/export/pgdata/9.1/data/
PGUSER=postgres
PGPORT=5433
PGDATABASE=postgres
PGHOST=localhost
CPPATH=/home/postgres/check_postgres.pl
export TERM=xterm
LOGFILES=`$BIN/psql -U postgres -d postgres -w -Atc "show log_directory;"`'/postgresql-'`date "+%Y-%m-%d"`*
#`$BIN/psql -U postgres -d postgres -w -Atc "show data_directory;"`'/'`$BIN/psql -U postgres -d postgres -w -Atc "show log_directory;"`'/postgresql-'`date "+%Y-%m-%d"`*
#LOGFILES=`$BIN/psql -U postgres -Atc  "show log_directory;"`/'postgresql-'`date "+%Y-%m-%d"`*

#########################################################################################

echo ""
printf "%100s\n" " " | tr ' ' '*'
printf "%30s\n" "                           ****   Daily Database Monitoring Report   ****"
printf "%100s\n" " " | tr ' ' '*'
echo -e "Server Date   : \t$(date)"
echo -e "Host Name     : \t$(hostname)"
echo -e "PG Start Time : \t `$BIN/psql -t -U $PGUSER -p $PGPORT -w -c "select to_char(pg_postmaster_start_time(),'FMMonth FMDDth YYYY at HH:MI:SS.MS');" -d template1`"
echo ""

#########################################################################################
### Disk space information which is above 60 %
#########################################################################################

printf "%100s\n" " " | tr ' ' '*'
echo "::Disk Information above 60 %::"
printf "%100s\n" " " | tr ' ' '*'
df -P | /bin/grep -vE '^Filesystem|ctfs|mnttab|proc|tmpfs|objfs|swap|platform|fd'| awk '{ print $5 " " $1 }' | while read output;
do
  usep=`echo $output | awk '{ print $1}' | cut -d '%' -f1`
  partition=`echo $output | awk '{ print $2 }'`
  if [ $usep -ge 60 ]; then
     echo "Partition : ($partition) ($usep %) as on `date`"
  fi
done
echo ""

#########################################################################################
### Check_postgres.pl checking on Vacuum/Max_connection/pg_xlog/transanction_wrapparound
#########################################################################################

printf "%100s\n" " " | tr ' ' '*'
echo "::Check Postgres Alerts & Status :: (Types - OK - WARNING - CRITICAL - UNKNOWN)"
printf "%100s\n" " " | tr ' ' '*'
echo " Database  |  Last_Vacuum | Max_Connections | Pg_xlog/.Ready | Tranx_Wrapparound |"
printf "%100s\n" " " | tr ' ' '*'
echo -e ""
for dbname in `$BIN/psql -Atc "select datname from pg_database where datname not in ('template0','template1');"`;
do
 # LAST_VACUUM=`$CPPATH -u $PGUSER -db $dbname -p $PGPORT --action last_vacuum --warning=7d --critical=10d  | awk '{print $2}'`
  MAX_CONN=`$CPPATH -u $PGUSER -db $dbname -p $PGPORT --warning=350 --critical=400 --action backends | awk '{print $2}'`
  PG_XLOG_STATUS=`$CPPATH -u $PGUSER -db $dbname -p $PGPORT --critical=64 --action archive_ready | awk '{print $2}'`
  TRX_WRAP=`$CPPATH -u $PGUSER -db $dbname -p $PGPORT --action txn_wraparound | awk '{print $2}'`
  echo "$dbname"
  echo -e "\t\t$LAST_VACUUM\t\t     $MAX_CONN\t\t  $PG_XLOG_STATUS\t\t  $TRX_WRAP\t"
done
echo ""

#########################################################################################
### Logs information on ERRORS/FATAL/PANIC/CHECKPOINT/INVALID MEMORY/SEGMENTATION
#########################################################################################

printf "%100s\n" " " | tr ' ' '*'
echo "::Log information ::"
printf "%100s\n" " " | tr ' ' '*'
echo -e "Errors             : \t`grep -i "error:" $LOGFILES | wc -l`"
echo -e "Invalid Memory     : \t`grep -i "invalid memory" $LOGFILES | wc -l`"
echo -e "FATAL              : \t`grep -i "fatal:" $LOGFILES | wc -l`"
echo -e "PANIC              : \t`grep -i "panic:" $LOGFILES | wc -l`"
echo -e "CHECKPOINT         : \t`grep -i "checkpoint are occurring too frequently" $LOGFILES | wc -l`"
echo -e "DEADLOCK           : \t`grep -i "deadlock" $LOGFILES | wc -l`"
echo -e "CORRUPTION         : \t`grep -i "compressed data is corrupt" $LOGFILES | wc -l`"
echo -e "Segmentation Fault : \t`grep -i "segmentation" $LOGFILES | wc -l`"
echo ""

#########################################################################################
### Any parameter changes inforamtion from a table.
#########################################################################################

printf "%100s\n" " " | tr ' ' '*'
echo ":: Database Parameter Changes ::"
printf "%100s\n" " " | tr ' ' '*'

if [ `$BIN/psql -U postgres -d postgres -Atc "select count(*) from para_change_check();"` -ne "0" ]; then
    $BIN/psql -U postgres -d postgres -Atc "select 'Parameter : '||name||'  -  '||setting from para_change_check();"
    echo ""
    echo "Note: After verifying, please update parameter table with the above changes"
    printf "%100s\n" " " | tr ' ' '*'
else
    echo -e "\t\t\t ***  No changes  ***"
fi
printf "%100s\n" " " | tr ' ' '*'
echo ""
echo "Note: Its General Database monitoring report. CRITICAL alerts are fixed in seperate alerting system."
echo "         Check_postgres.pl, WARNING:/CRITICAL:/UNKNOWN: should be monitored manually"
echo ""
}